Assessment: Creating a Responsive Photo Gallery
Today you will practice using Flexbox and CSS media queries to create a simple web site that adapts its layout depending on screen dimensions.

You will use Chrome Developer Tools Device Mode to test how your page will appear on mobile devices.

Your site will be a photo gallery consisting of at least 10 photos. Feel free to use any photos you like. If you aren't sure what to use, you are welcome to grab some pictures of puppies and kittens off Google images. Convert all the photos to be 236 pixels wide JPGs (remember how you learned to use the ImageMagick convert tool to quickly change the dimensions of an image?).

See below for rough sketches of the layouts you should use for your photo gallery depending on whether it is viewed on mobile or on a wider screen. Feel free to apply what you've learned to customize the appearance of your site beyond the layout requirements described below.

Mobile Layout
The mobile layout should have a full-width header, followed by a gallery of photographs presented in two columns.

gallery-mockup-mobile.jpg

Desktop Layout
The layout you use on wider screens will have all the same elements, but will place the header on the left and use a varying number of columns of photos depending on the screen width.

gallery-mockup-desktop.jpg

